package com.siletti;

import com.siletti.gcm.QuestionsGame;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class App {

    private static final String WELCOME = " == Welcome! == ";
    private static final String GOODBYE = " == Goodbye! == ";
    private static final String PROMPTING = "Please enter:  <question>? [\"<answer1>\" \"<answer2>\"...] "
            + " Press Enter to exit";

    public static void main(String[] args) {
        QuestionsGame questionsGame = new QuestionsGame(new HashMap<>());
        Scanner scanner = new Scanner(System.in);
        System.out.println(WELCOME);
        System.out.println(PROMPTING);
        while (true) {
            String inputLine = scanner.nextLine().trim();
            // check if the user wants to exit
            if (inputLine.length() == 0) {
                System.out.println(GOODBYE);
                break;
            }
            // Parse input line in  <theQuestion, List<answer>>
            Map.Entry<String, List<String>> parsedInputLine = questionsGame.parseInputLine(inputLine);
            if (parsedInputLine == null) {
                System.out.println(PROMPTING);
                continue;
            }
            // Execute the question and print results
            questionsGame.buildTheAnswer(parsedInputLine).forEach(System.out::println);
        }
        scanner.close();
    }

}
