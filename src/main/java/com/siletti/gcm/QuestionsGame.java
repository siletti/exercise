package com.siletti.gcm;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class QuestionsGame {

    static final String DEFAULT_ANSWER = "the answer to life, universe and everything is 42";
    static final int MAX_LENGTH = 255;
    static final String ADDED_MSG = "== Added == ";

    // The correct question contain only one ending '?'
    private static final String REGEX_QUESTION = "[^?]+\\?";

    // Question with answers, like "<answer1>" "<answer2>" ..
    private static final String REGEX_WITH_ANSWERS = "[^?)]+\\?(\\s+\"[^\"]+\")+";

    private final Map<String, List<String>> questionsDB;

    public QuestionsGame(Map<String, List<String>> questionsDB) {
        this.questionsDB = questionsDB;
    }

    // Extract question and any answers
    public Map.Entry<String, List<String>> parseInputLine(String inputLine) {
        // check question/answers syntax
        inputLine = inputLine.trim();
        if (!(inputLine.matches(REGEX_QUESTION) | inputLine.matches(REGEX_WITH_ANSWERS))) {
            return null;
        }
        String[] result = inputLine.split("\\?");
        String theQuestion = result[0];
        // check question length limit
        if (!checkLengthLimit(theQuestion)) {
            return null;
        }
        if (inputLine.matches(REGEX_QUESTION)) {
            return new AbstractMap.SimpleEntry<>(theQuestion, null);
        } else {
            //handle question with answers
            List<String> answersList = new ArrayList<>();
            String[] answersArray = result[1].split("\"");
            for (int i = 1; i < answersArray.length; i = i + 2) {
                // check answer length limit
                if (checkLengthLimit(answersArray[i])) {
                    answersList.add(answersArray[i]);
                }
            }
            if (answersList.size() == 0) {
                // there is no answer with correct length
                return null;
            } else {
                return new AbstractMap.SimpleEntry<>(theQuestion, answersList);
            }
        }
    }

    // Retrieve the answers or add the new ones
    public List<String> buildTheAnswer(Map.Entry<String, List<String>> map) {
        List<String> result = new ArrayList<>();
        String theQuestion = map.getKey();
        if (map.getValue() == null) {
            // it is a simple question
            if (!questionsDB.containsKey(theQuestion)) {
                result.add(DEFAULT_ANSWER);
            } else {
                result = questionsDB.get(theQuestion);
            }
        } else {
            // it is a question with answers
            questionsDB.put(map.getKey(), map.getValue());
            result.add(ADDED_MSG);
        }
        return result;
    }

    private boolean checkLengthLimit(String s) {
        return s.length() <= MAX_LENGTH;
    }

}