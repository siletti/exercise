package com.siletti.gcm;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class QuestionsGameTest {

    private static final String TEST_LINE = "What is Peters favorite food? \"Pizza\" \"Spaghetti\" \"Ice cream\"";
    private QuestionsGame questionsGame;

    @Before
    public void setUp()  {
        Map<String, List<String>> map = new HashMap<>();
        map.put("What is Peters favorite food", Arrays.asList("Pizza", "Spaghetti", "Ice cream"));
        questionsGame = new QuestionsGame(map);
    }

    @Test
    public void parseInputLineTest() {
        Map.Entry<String, List<String>> inputLine = questionsGame.parseInputLine(TEST_LINE);
        assertEquals("What is Peters favorite food", inputLine.getKey());
        assertEquals(inputLine.getValue(), Arrays.asList("Pizza", "Spaghetti", "Ice cream"));

        inputLine = questionsGame.parseInputLine("Simple question?");
        assertEquals("Simple question", inputLine.getKey());
        assertNull(inputLine.getValue());

        assertNull(questionsGame.parseInputLine(" without question-mark "));
        assertNull(questionsGame.parseInputLine(" question ? with bad formatted answer  "));
        assertNull(questionsGame.parseInputLine(" question ? \"ok\" \"with bad answers  "));

        // question with too much chars
        String questionTooLong =  new String(new char[QuestionsGame.MAX_LENGTH+1]).replace("\0", "q") + "?";
        assertNull(questionsGame.parseInputLine(questionTooLong));

        // answer with too much chars
        String answerTooLong =  new String(new char[QuestionsGame.MAX_LENGTH+2]).replace("\0", "a") ;
        String withAnswerTooLong = "Simple question? " + "\"" + answerTooLong + "\"";
        assertNull(questionsGame.parseInputLine(withAnswerTooLong));

    }

    @Test
    public void buildTheAnswerTest() {

        // the question already exists
        List <String> stringList = questionsGame.buildTheAnswer(questionsGame.parseInputLine("What is Peters favorite food?"));
        assertEquals(stringList, Arrays.asList("Pizza", "Spaghetti", "Ice cream"));

        // the question does not exist
        stringList = questionsGame.buildTheAnswer(questionsGame.parseInputLine("What is the answer to life?"));
        assertEquals(stringList, Arrays.asList(QuestionsGame.DEFAULT_ANSWER));

        // A new question with answers
        Map.Entry<String, List<String>> inputLine = questionsGame.parseInputLine("A new question? \"answer1\" \"answer2\"");
        stringList = questionsGame.buildTheAnswer(inputLine);
        assertEquals(stringList, Arrays.asList(QuestionsGame.ADDED_MSG));
        stringList = questionsGame.buildTheAnswer(questionsGame.parseInputLine("A new question?"));
        assertEquals(stringList, Arrays.asList("answer1", "answer2"));

    }
}